/* Environment includes. */
#include "DriverLib.h"

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include <string.h>

/* UART configuration - note this does not use the FIFO so is not very
efficient. */
#define mainBAUD_RATE                   ( 19200 )
#define mainFIFO_SET                    ( 0x10 )

/* Task priorities. */
#define mainTasksSensor_PRIORITY        (tskIDLE_PRIORITY + 1)
#define mainTasksFilter_PRIORITY        (tskIDLE_PRIORITY + 1)
#define mainTasksPrint_PRIORITY         (tskIDLE_PRIORITY + 1)
#define mainTasksInfo_PRIORITY          (tskIDLE_PRIORITY + 3)

/* Misc. */
#define mainQUEUE_SIZE                  ( 10 )
#define mainISR_SIZE                    (  1 )
#define mainBUFF_SIZE                   ( 10 )
#define mainLED_SIZE                    ( 96 )
#define mainINFO_SIZE                   ( 512 )

/*
 * Configure the processor and peripherals for this demo.
 */
static void prvSetupHardware(void);

/*
 * Read data from sensor.
 */
_Noreturn static void vSensor(void *pvParameter);

/*
 * Filter data received from sensor.
 */
_Noreturn static void vFilter(void *pvParameter);

/*
 * The task that controls access to the LCD.
 */
_Noreturn static void vPrintTask(void *pvParameter);

/*
 * Convert a T° in one bit of the display LED.
 */
static uint8_t normalize(uint8_t m);

/*
 * Task that sends system info.
 */
_Noreturn static void vTasksInfo(void *pvParameter);

/*
 * Auxiliary method to send strings via UART.
 */
static void uart_print(char *msg, size_t len);

/* The queue used to store/reads from sensor. */
QueueHandle_t xTemperatureQueue;

/* The queue used to store/reads from UART. */
QueueHandle_t xRxedChars;

/* String that is transmitted on the UART. */
static volatile char *pcNextChar;

/* Delays. */
const TickType_t xDelay10ms = pdMS_TO_TICKS(10);
const TickType_t xDelay1s = pdMS_TO_TICKS(1000);

/* Sensor values buffer. */
uint8_t val[mainBUFF_SIZE];

/* LCD buffers. */
uint8_t bitmap1[mainLED_SIZE];
uint8_t bitmap2[mainLED_SIZE];

/*-----------------------------------------------------------*/

int main(void) {
    /* Configure the clocks, UART and GPIO. */
    prvSetupHardware();

    /* Necessary to vTaskGetRunTimeStats function. */
    vSetupHighFrequencyTimer();

    /* Clear LCD buffers. */
    memset(bitmap1, 0, mainLED_SIZE);
    memset(bitmap2, 0, mainLED_SIZE);

    /* Create the queue used to pass message to vFilter. */
    xTemperatureQueue = xQueueCreate(mainQUEUE_SIZE, sizeof(uint8_t));
    xRxedChars = xQueueCreate(mainISR_SIZE, sizeof(uint8_t));

    /* Start the tasks defined within the file. */
    xTaskCreate(vSensor, "Sensor", configMINIMAL_STACK_SIZE, NULL, mainTasksSensor_PRIORITY, NULL);
    xTaskCreate(vFilter, "Filter", configMINIMAL_STACK_SIZE, NULL, mainTasksFilter_PRIORITY, NULL);
    xTaskCreate(vPrintTask, "Print", configMINIMAL_STACK_SIZE, NULL, mainTasksPrint_PRIORITY, NULL);
    xTaskCreate(vTasksInfo, "TasksInfo", 220, NULL, mainTasksInfo_PRIORITY, NULL);

    /* Start the scheduler. */
    vTaskStartScheduler();

    /* Will only get here if there was insufficient heap to start the
    scheduler. */

    return 0;
}

/*-----------------------------------------------------------*/

/**
 * Tarea encargada de simular un sensor de temperatura.
 * Genera valores de temperatura con una frecuencia de 100 Hz (10ms).
 * @note se restringen los valores de funcionamiento del sensor dentro
 *      del rango [0-40]°C.
 * @param pvParameter
 */
_Noreturn static void vSensor(void *pvParameter) {
    uint8_t i = 0;
    TickType_t xLastWakeTime;
    xLastWakeTime = xTaskGetTickCount();

    UBaseType_t uxHighWaterMark;
    uxHighWaterMark = uxTaskGetStackHighWaterMark(NULL);

    for (;;) {
        xQueueSend(xTemperatureQueue, &i, portMAX_DELAY);
        vTaskDelayUntil(&xLastWakeTime, xDelay10ms);
        if (i == 40) {
            i = 0;
        } else {
            i++;
        }
        uxHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
    }
}

/*-----------------------------------------------------------*/

/**
 * Tarea encargada de registrar los valores leídos del sensor,
 * realizar el promedio móvil de las últimas N temperaturas,
 * donde N es un valor ventana que puede ser modificado
 * ingresando los comandos correspondientes desde la UART.
 * Además, esta tarea agrega el nuevo valor de la media móvil
 * normalizado a los buffers para ser mostrado por display.
 * @note Se pueden almacenar hasta 10 lecturas del sensor.
 * @note El tamaño de la ventana puede variar entre [1-10].
 * @note Para modificar el tamaño de ventana se deben enviar
 * los caracteres '+' o '-' vía UART.
 * @note Si el promedio móvil es > 20 se setea el led correspondiente
 * en el buffer de la parte superior del display, mientras que
 * si es < 20, se setea en el buffer inferior.
 * @param pvParameter
 */
_Noreturn static void vFilter(void *pvParameter) {
    uint8_t pcMessage;
    uint16_t mean;
    uint8_t window = mainBUFF_SIZE;
    UBaseType_t upd_window;

    UBaseType_t uxHighWaterMark;
    uxHighWaterMark = uxTaskGetStackHighWaterMark(NULL);

    for (;;) {
        /* Wait for a message to arrive. */
        if (xQueueReceive(xTemperatureQueue, &pcMessage, portMAX_DELAY) == pdPASS) {
            for (int i = mainBUFF_SIZE - 1; i > 0; i--) {
                val[i] = val[i - 1];
            }
            val[0] = pcMessage;
        }
        /* Shift left bitmap buffers */
        for (int i = 0; i < mainLED_SIZE - 1; i++) {
            bitmap1[i] = bitmap1[i + 1];
            bitmap2[i] = bitmap2[i + 1];
        }
        /* Update window value */
        if (xQueueIsQueueFullFromISR(xRxedChars)) {
            if (xQueueReceiveFromISR(xRxedChars, &upd_window, NULL) == pdPASS) {
                if (upd_window == '+' && window < mainBUFF_SIZE) {
                    window++;
                } else if (upd_window == '-' && window > 1) {
                    window--;
                }
            }
        }
        /* Calculate new mean */
        mean = 0;
        for (int i = 0; i < window; i++) {
            mean += val[i];
        }
        mean /= window;
        /* Set LED bit on Display */
        if (mean < 20) {
            bitmap1[mainLED_SIZE - 1] = 0;
            bitmap2[mainLED_SIZE - 1] = normalize(mean) | 0b10000000;
        } else {
            bitmap1[mainLED_SIZE - 1] = normalize(mean - (uint8_t) 20);
            bitmap2[mainLED_SIZE - 1] = 0b10000000;
        }
        uxHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
    }
}

/*-----------------------------------------------------------*/

/**
 * Función auxiliar para corresponder el valor de T° con un led del display.
 * @note La correspondencia es la siguiente:
 * @verbatim
 *  +-------+       °C
 *  |   | 0 |  -> [18-20]
 *  | B | 1 |  -> [16-17]
 *  | y | 2 |  -> [13-15]
 *  | t | 3 |  -> [11-12]
 *  | e | 4 |  -> [ 8-10]
 *  |   | 5 |  -> [ 6-7 ]
 *  | 0 | 6 |  -> [ 3-5 ]
 *  |   | 7 |  -> [ 0-2 ]
 *  +-------+
 * @endverbatim
 * @example En caso que m = 14, se seteará el bit 2, lo que hace
 * encender únicamente el led correspondiente del display.
 * @param m valor a convertir.
 * @return valor binario con el bit correspondiente en alto.
 */
static uint8_t normalize(uint8_t m) {
    if (m >= 0 && m <= 2)
        return 0b10000000;
    else if (m > 2 && m <= 5)
        return 0b01000000;
    else if (m > 5 && m <= 7)
        return 0b00100000;
    else if (m > 7 && m <= 10)
        return 0b00010000;
    else if (m > 10 && m <= 12)
        return 0b00001000;
    else if (m > 12 && m <= 15)
        return 0b00000100;
    else if (m > 15 && m <= 17)
        return 0b00000010;
    else if (m > 17 && m <= 20)
        return 0b00000001;
    else
        return 0;
}

/*-----------------------------------------------------------*/

/**
 * Configuración inicial del HW.
 * Se inicializa el display y la UART.
 * Se activan las interrupciones RX y TX de la UART.
 */
static void prvSetupHardware(void) {
    /* Setup the PLL. */
    SysCtlClockSet(SYSCTL_SYSDIV_10 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_6MHZ);

    /* Enable the UART.  */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    /* Set GPIO A0 and A1 as peripheral function.  They are used to output the
    UART signals. */
    GPIODirModeSet(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1, GPIO_DIR_MODE_HW);

    /* Configure the UART for 8-N-1 operation. */
    UARTConfigSet(UART0_BASE, mainBAUD_RATE, UART_CONFIG_WLEN_8 | UART_CONFIG_PAR_NONE | UART_CONFIG_STOP_ONE);

    /* We don't want to use the fifo.  This is for test purposes to generate
    as many interrupts as possible. */
    HWREG(UART0_BASE + UART_O_LCR_H) &= ~mainFIFO_SET;

    /* Enable Tx and Rx interrupts. */
    HWREG(UART0_BASE + UART_O_IM) |= UART_INT_TX;
    HWREG(UART0_BASE + UART_O_IM) |= UART_INT_RX;
    IntPrioritySet(INT_UART0, configKERNEL_INTERRUPT_PRIORITY);
    IntEnable(INT_UART0);

    /* Initialise the LCD> */
    OSRAMInit(false);
    OSRAMStringDraw("www.FreeRTOS.org", 0, 0);
    OSRAMStringDraw("LM3S811 demo", 16, 1);
}

/*-----------------------------------------------------------*/

/**
 * Handler de UART.
 * @note Si la interrupción es por RX, recibe el valor ingresado
 * y en caso de ser alguno de los comandos válidos ('+' o '-'),
 * lo inserta en la cola de comandos recibidos para que la tarea de filtro
 * modifique el valor de la ventana según corresponda.
 * @note Si la interrupción es por TX se envían los caracteres indicados.
 */
void vUART_ISR(void) {
    unsigned long ulStatus;

    /* What caused the interrupt. */
    ulStatus = UARTIntStatus(UART0_BASE, pdTRUE);

    /* Clear the interrupt. */
    UARTIntClear(UART0_BASE, ulStatus);

    /* Was a Tx interrupt pending? */
    if (ulStatus & UART_INT_TX) {
        /* Send the next character in the string.  We are not using the FIFO. */
        if (*pcNextChar != 0) {
            if (!(HWREG(UART0_BASE + UART_O_FR) & UART_FR_TXFF)) {
                HWREG(UART0_BASE + UART_O_DR) = *pcNextChar;
            }
            pcNextChar++;
        }
    }
    /* Was a Rx interrupt pending? */
    else if (ulStatus & UART_INT_RX) {
        signed char cChar;
        portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

        /* A character was received.  Place it in the queue of received characters. */
        cChar = UARTCharGet(UART0_BASE);
        if (cChar == '+' || cChar == '-') {
            xQueueSendFromISR(xRxedChars, &cChar, &xHigherPriorityTaskWoken);
        }
        portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
    }
}

/*-----------------------------------------------------------*/

/**
 * Tarea encargada de mostrar la gráfica de los promedios ponderados de las °T.
 * @note Muestra el eje horizontal que se corresponde con 0°C.
 * @note El display tiene una tasa de refresco de 1 seg.
 * @param pvParameters
 */
static void vPrintTask(void *pvParameters) {
    TickType_t xLastWakeTime;
    xLastWakeTime = xTaskGetTickCount();

    UBaseType_t uxHighWaterMark;
    uxHighWaterMark = uxTaskGetStackHighWaterMark(NULL);

    for (;;) {
        /* Write the message to the LCD. */
        OSRAMClear();
        OSRAMImageDraw(bitmap1, 0, 0, mainLED_SIZE, 1);
        OSRAMImageDraw(bitmap2, 0, 1, mainLED_SIZE, 1);
        vTaskDelayUntil(&xLastWakeTime, xDelay1s);
        uxHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
    }
}

/*-----------------------------------------------------------*/

/**
 * Tarea encargada de transmitir via UART las estadísticas del
 * sistema. Para esto utiliza las funciones 'vTaskList' y
 * 'vTaskGetRunTimeStats'.
 * @note 'vTaskList' muestra el listado de las tareas con su
 * estado, prioridad, máximo stack usado (HighWaterMark) y el ID.
 * @note 'vTaskGetRunTimeStats' muestra el tiempo total de ejecución
 * de cada tarea expresado como valor absoluto y en porcentaje.
 * @param pvParameter
 */
_Noreturn static void vTasksInfo(void *pvParameter)
{
    TickType_t xLastWakeTime;
    xLastWakeTime = xTaskGetTickCount();

    UBaseType_t uxHighWaterMark;
    uxHighWaterMark = uxTaskGetStackHighWaterMark(NULL);

    char *headerTaskList = "Task\t\tState\tPrio\tStack\tNum";
    char *headerRunTimeStats = "Task\t\tABS\t\t%";
    char *line = "*********************************************";
    char taskInfo[mainINFO_SIZE];

    memset(taskInfo, 0, mainINFO_SIZE);

    for (;;)
    {
        uart_print("\n",1);
        uart_print(line, strlen(line));
        uart_print(headerTaskList, strlen(headerTaskList));
        uart_print(line, strlen(line));
        vTaskList(taskInfo);
        uart_print(taskInfo, strlen(taskInfo));
        uart_print(line, strlen(line));
        uart_print(headerRunTimeStats, strlen(headerRunTimeStats));
        uart_print(line, strlen(line));
        vTaskGetRunTimeStats(taskInfo);
        uart_print(taskInfo, strlen(taskInfo));
        uart_print(line, strlen(line));

        vTaskDelayUntil(&xLastWakeTime, xDelay1s);
        uxHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
    }
}

/*-----------------------------------------------------------*/

/**
 * Función auxiliar usada por la tarea vTaskInfo para enviar
 * una cadena de caracteres vía UART.
 * @param msg cadena a enviar.
 * @param len longitud de la cadena a enviar.
 */
static void uart_print(char *msg, size_t len)
{
    for (int i = 0; i < len; i++)
    {
        UARTCharPut(UART0_BASE, msg[i]);
    }
    UARTCharPut(UART0_BASE, '\n');
    UARTCharPut(UART0_BASE, '\r');
}
