## Directories:

+ The **FreeRTOS/Source** directory contains the FreeRTOS source code, and contains
  its own readme file.

+ The **FreeRTOS/Demo** directory contains a demo application for every official
FreeRTOS port, and contains its own readme file.
  
See [FreeRTOS/SourceOrganization](http://www.freertos.org/a00017.html) for full details of the directory structure and information on locating the files you require.

The easiest way to use FreeRTOS is to start with one of the pre-configured demo 
application projects (found in the FreeRTOS/Demo directory).  That way you will
have the correct FreeRTOS source files included, and the correct include paths
configured.  
Once a demo application is building and executing you can remove
the demo application file, and start to add in your own application source
files.

### Commands:
``qemu-system-arm -machine lm3s811evb -cpu cortex-m3 -kernel gcc/RTOSDemo.axf -gdb tcp::3333 --serial stdio``

``arm-none-eabi-gdb gcc/RTOSDemo.axf``

### See also -
+ [Quick Start Guide](http://www.freertos.org/FreeRTOS-quick-start-guide.html)

+ [FAQ](http://www.freertos.org/FAQHelp.html)
